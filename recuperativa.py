#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json


# funcion crea archivos json
def crear_json(lista):
    # variables con dic para cada flor
    dataS = {}
    dataS["flores"] = []

    dataVE = {}
    dataVE["flores"] = []

    dataVI = {}
    dataVI["flores"] = []
    # recorremos la lista comprobando el indice especies
    # agregando al dic correspondiente
    for i in lista:
        if i["species"] == "setosa":

            dataS["flores"].append({
                "nombre": i["species"],
                "largosepalo": i["sepalLength"],
                "anchosepalo": i["sepalWidth"],
                "largopetalo": i["petalLength"],
                "anchopetalo": i["petalWidth"]
                })

        elif i["species"] == "versicolor":

            dataVE["flores"].append({
                "nombre": i["species"],
                "largosepalo": i["sepalLength"],
                "anchosepalo": i["sepalWidth"],
                "largopetalo": i["petalLength"],
                "anchopetalo": i["petalWidth"]
                })

        else:

            dataVI["flores"].append({
                "nombre": i["species"],
                "largosepalo": i["sepalLength"],
                "anchosepalo": i["sepalWidth"],
                "largopetalo": i["petalLength"],
                "anchopetalo": i["petalWidth"]
                })
    # se crean los archivos json
    with open("setosa.json", 'w') as file:
        json.dump(dataVI, file, indent=4)
    with open("virginica.json", 'w') as file:
        json.dump(dataVI, file, indent=4)
    with open("versicolor.json", 'w') as file:
        json.dump(dataVE, file, indent=4)


# cargamos el archivo para trabajarlo
def cargar_fichero():

    with open("iris.json", "r") as file:
        archivo = json.load(file)
    # retornamos guardando datos
    return archivo


# pasamos los datos a un lista para desarrollarla
def lista_archivo(datos):

    list = []
    # se agrega cada elemento del archivo
    for i in datos:
        list.append(i)
    # print(list)
    # print(type(list))
    return list


# se imprimen el species con su respectivo valor
def especies_encontradas(lista):

    especies = []
    # se crea una especie de sub lista para eliminar elementos repetidos
    for i in lista:
        especie = i["species"]
        if especie not in especies:
            especies.append(especie)
        else:
            pass
    # se imprime lista sin elementos repetidos
    for i in especies:
        print("species:", i)


def setosa_promedios(lista):

    prom_petalL = []
    prom_petalW = []

    # recorremos la lista
    for i in lista:
        if i["species"] == "setosa":
            enteroL = float(i["petalLength"])
            prom_petalL.append(enteroL)
            enteroW = float(i["petalWidth"])
            prom_petalW.append(enteroW)
    # print(prom_petal)
    # se calcula el promedio por metodo sum dividido
    # en el largo de la sub lista
    promedioL = sum(prom_petalL) / len(prom_petalL)
    promedioW = sum(prom_petalW) / len(prom_petalW)
    # print(prom_sepal)
    print("EN SETOSAS:")
    print("El promedio del largo es:", promedioL)
    print("El promedio del ancho es:", promedioW)
    return promedioL, promedioW


def versicolor_promedios(lista):

    prom_petalL = []
    prom_petalW = []

    # recorremos la lista
    for i in lista:
        if i["species"] == "versicolor":
            enteroL = float(i["petalLength"])
            prom_petalL.append(enteroL)
            enteroW = float(i["petalWidth"])
            prom_petalW.append(enteroW)
    # print(prom_petal)
    # se calcula el promedio por metodo sum
    # dividido en el largo de la sub lista
    promedioL = sum(prom_petalL) / len(prom_petalL)
    promedioW = sum(prom_petalW) / len(prom_petalW)
    # print(prom_sepal)
    print("EN VERSICOLOR:")
    print("El promedio del largo es:", promedioL)
    print("El promedio del ancho es:", promedioW)
    return promedioL, promedioW


def virginica_promedios(lista):

    prom_petalL = []
    prom_petalW = []

    # recorremos la lista
    for i in lista:
        if i["species"] == "virginica":
            enteroL = float(i["petalLength"])
            prom_petalL.append(enteroL)
            enteroW = float(i["petalWidth"])
            prom_petalW.append(enteroW)
    # print(prom_petal)
    # se calcula el promedio por metodo sum
    # dividido en el largo de la sub lista
    promedioL = sum(prom_petalL) / len(prom_petalL)
    promedioW = sum(prom_petalW) / len(prom_petalW)
    # print(prom_sepal)
    print("EN VIRGINICAS:")
    print("El promedio del largo es:", promedioL)
    print("El promedio del ancho es:", promedioW)
    return promedioL, promedioW


# funcion que compara para parametro entregado
def petalos_mas_ancho(promediowS, promediowVE, promediowVI):

    # promedio de setosas seria el mayor
    if promediowS > promediowVE and promediowS > promediowVI:
        print("Petalos mas anchos es la setosa:", promediowS)
    # promedio de versicolor seria el mayor
    elif promediowVE > promediowS and promediowVE > promediowVI:
        print("Petalos mas anchos es la versicolor:", promediowVE)
    # promedio de virginica seria el mayor
    else:
        print("Petalos mas anchos es virginica:", promediowVI)


def petalos_mas_altos(promediolS, promediolVE, promediolVI):

    # promedio de setosas seria el mayor
    if promediolS > promediolVE and promediolS > promediolVI:
        print("Petalos mas anchos es la setosa:", promediolS)
    # promedio de versicolor seria el mayor
    elif promediolVE > promediolS and promediolVE > promediolVI:
        print("Petalos mas anchos es la versicolor:", promediolVE)
    # promedio de virginica seria el mayor
    else:
        print("Petalos mas largos es virginica:", promediolVI)


def max_cada_especie(lista):

    sepalo_setosa = []
    sepalo_versicolor = []
    sepalo_virginicas = []

    for i in lista:
        if i["species"] == "setosa":
            decimalS = float(i["sepalLength"])
            sepalo_setosa.append(decimalS)
    # print(sepalo_setosa)
    maximoS = max(sepalo_setosa)
    # print(maximoS)

    for i in lista:
        if i["species"] == "versicolor":
            decimalVE = float(i["sepalLength"])
            sepalo_versicolor.append(decimalVE)
    maximoVE = max(sepalo_versicolor)
    # print(maximoVE)

    for i in lista:
        if i["species"] == "virginica":
            decimalVI = float(i["sepalLength"])
            sepalo_virginicas.append(decimalVI)
    # print(sepalo_setosa)
    maximoVI = max(sepalo_virginicas)
    # print(maximoVI)

    return maximoS, maximoVE, maximoVI


def max_sepalo(sepS, sepVE, sepVI):

    # promedio de setosas seria el mayor
    if sepS > sepVE and sepS > sepVI:
        print("el maximo de un sepalo mas alto es:", sepS)
        print("corresponde a la setosa")
    # promedio de versicolor seria el mayor
    elif sepVE > sepS and sepVE > sepVI:
        # promedio de virginica seria el mayor
        print("el maximo de un sepalo mas alto es:", sepVE)
        print("corresponde a la versicolor")
    else:
        print("el maximo de un cepalo es:", sepVI)
        print("corresponde a la virginica")


def menu():
    datos = cargar_fichero()
    # print(datos)
    lista = lista_archivo(datos)
    print()
    especies_encontradas(lista)
    print()
    # 3 funcioes que complico que sean 1 sola
    promediolS, promediowS = setosa_promedios(lista)
    print()
    promediolVE, promediowVE = versicolor_promedios(lista)
    print()
    promediolVI, promediowVI = virginica_promedios(lista)
    print()
    petalos_mas_ancho(promediowS, promediowVE, promediowVI)
    petalos_mas_altos(promediolS, promediolVE, promediolVI)
    print()
    sepS, sepVE, sepVI = max_cada_especie(lista)
    max_sepalo(sepS, sepVE, sepVI)
    crear_json(lista)


if __name__ == '__main__':
    menu()
